/**
 * @author Bertie Ancona
 */
package mypackage;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import org.lwjgl.opengl.Display;

public abstract class GameObjectType {
	protected final String name;
	protected final String texture;

	protected GameObjectType(String name, String texture) {
		this.name = name;
		this.texture = texture;
	}

	protected String getTypesJSON() {
		String typesJSON = null;
		try {
			typesJSON = new String(Files.readAllBytes(Paths
					.get(getClass().getResource(
							"/json/gameobjecttypes.json").toURI())));
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
			Display.destroy();
			System.exit(1);
		}
		return typesJSON;
	}

	public String getName() {
		return name;
	}

    public GameTexture getTextureObj() {
        return GameTexture.get(name);
    }

    private static Map<String, GameObjectType> types;
    private static boolean loaded = false;

    public static GameObjectType get(String name) {
        if (!loaded) {
            loadTypes();
        }
        return types.get(name);
    }

    public static void loadTypes() {
        if (loaded) {
            return;
        }
        loaded = true;
        types = new HashMap<>();
        String typesJSON = null;
        try {
            typesJSON = new String(Files.readAllBytes(Paths
                    .get(ActorType.class.getResource(
                            "/json/gameobjecttypes.json").toURI())));
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            Display.destroy();
            System.exit(1);
        }
        Gson gson = new Gson();
        JsonArray actorTypes = new JsonParser().parse(typesJSON)
                .getAsJsonObject().get("actorTypes").getAsJsonArray();
        for (int i = 0; i < actorTypes.size(); ++i) {
            ActorType actorType = gson.fromJson(actorTypes.get(i)
                    .getAsJsonObject(), ActorType.class);
            types.put(actorType.getName(), actorType);
        }
        JsonArray tileTypes = new JsonParser().parse(typesJSON)
                .getAsJsonObject().get("tileTypes").getAsJsonArray();
        for (int i = 0; i < tileTypes.size(); ++i) {
            TileType tileType = gson.fromJson(tileTypes.get(i)
                    .getAsJsonObject(), TileType.class);
            types.put(tileType.getName(), tileType);
        }
    }

}