/**
 * @author Bertie Ancona
 */
package mypackage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Tile extends GameObject {

	// Tile size
	public static final double WIDTH = 128;
	public static final double HEIGHT = 64;
	public static final double HALF_WIDTH = WIDTH * 0.5;
	public static final double HALF_HEIGHT = HEIGHT * 0.5;

	// Effects imposed on this Tile
	private ArrayList<TileEffect> addedEffects;
	private final ArrayList<TileEffect> permanentEffects;
	
	// Boolean properties of Tile
	private boolean hover, selected, reachable;
	
	// Actor currently on Tile or null if none present
	private Actor actorOnTile;
	
	// Adjacent Tiles
	private Tile nextUp, nextDown, nextLeft, nextRight;
	private final Tile[] adjacentTiles = new Tile[4];

	public Tile(TileType type, Actor actorOnTile, TileEffect... effects) {
		super(type);
		this.actorOnTile = actorOnTile;
		addedEffects     = new ArrayList<TileEffect>();
		permanentEffects = new ArrayList<TileEffect>(Arrays.asList(effects));
	}
	
	public Tile(TileType type, TileEffect... effects) {
		this(type, null, effects);
	}

	public void paint() {
		double[] texCoords = getType().getTextureObj().getTexCoords(getState());
		paint(texCoords);

		if (actorOnTile != null)
			actorOnTile.paint();
	}

	public Set<Tile> getReachableTiles(double speed) {
		return getReachableTiles(speed + 1, this, null);
	}

	private Set<Tile> getReachableTiles(double speed, 
										Tile startTile, 
										Set<Tile> checkedTiles) {
		Set<Tile> reachableTiles = new HashSet<Tile>();
		reachableTiles.add(this);
		checkedTiles = (checkedTiles == null) ?
						new HashSet<Tile>() : checkedTiles;
		checkedTiles.add(this);
		speed -= getType().traversalTime;
		if (speed > 0) {
			for (Tile adjTile : adjacentTiles) {
				if (adjTile != null && !checkedTiles.contains(adjTile)) {
					reachableTiles.addAll(
						adjTile.getReachableTiles(speed,
												  startTile, 
												  checkedTiles));
				}
			}
		}
		reachableTiles.remove(startTile);
		return reachableTiles;
	}

	public Set<Tile> getTilesInRange(int range) {
		return getTilesInRange(range + 1, this, null);
	}

	private Set<Tile> getTilesInRange(int range, 
									  Tile startTile, 
									  Set<Tile> checkedTiles) {
		Set<Tile> tilesInRange = new HashSet<Tile>();
		tilesInRange.add(this);
		checkedTiles = (checkedTiles == null) ? 
						new HashSet<Tile>() : checkedTiles;
		checkedTiles.add(this);
		if (--range > 0) {
			for (Tile adjTile : adjacentTiles) {
				if (adjTile != null && !checkedTiles.contains(adjTile)) {
					tilesInRange.addAll(
						adjTile.getTilesInRange(range,
												startTile, 
												checkedTiles));
				}
			}
		}
		tilesInRange.remove(startTile);
		return tilesInRange;
	}

	@Override
	public List<String> getHUDDescriptors() {
		List<String> descriptors = super.getHUDDescriptors();
		Collections.addAll(
			descriptors, 
			"Traversal Time: " + getType().traversalTime,
			"Actor On Tile: " + actorOnTile == null ? 
				"None" : actorOnTile.getType().getName());
		return descriptors;
	}

	public TileState getState() {
		return hover ? TileState.HOVER
				: this == HUD.getGameObjectInFocus() ? TileState.IN_FOCUS
						: reachable ? TileState.REACHABLE 
								: TileState.REGULAR;
	}

	/**
	 * @return the permanentEffects
	 */
	public List<TileEffect> getPermanentEffects() {
		return Collections.unmodifiableList(permanentEffects);
	}

	/**
	 * @return the addedEffects
	 */
	public List<TileEffect> getAddedEffects() {
		return Collections.unmodifiableList(addedEffects);
	}

	/**
	 * @return the addedEffects and the permanentEffects
	 */
	public List<TileEffect> getActiveEffects() {
		ArrayList<TileEffect> activeEffects = new ArrayList<TileEffect>();
		activeEffects.addAll(permanentEffects);
		activeEffects.addAll(addedEffects);
		return Collections.unmodifiableList(activeEffects);
	}

	/**
	 * @param effect
	 *            the effects to add
	 * @return true if addition successful, false if effect could not be added
	 */
	public boolean addEffect(TileEffect effect) {
		if (effect.canStack || !getActiveEffects().contains(effect)) {
			addedEffects.add(effect);
			return true;
		}
		return false;
	}

	/**
	 * @param effect
	 *            the effects to remove
	 * @return true if removal successful, false if effect was not active in
	 *         tile
	 */
	public boolean removeEffect(TileEffect effect) {
		return addedEffects.remove(effect);
	}

	public boolean isHover() {
		return hover;
	}

	public boolean isSelected() {
		return selected;
	}

	public boolean isReachable() {
		return reachable;
	}

	public void setHover(boolean hover) {
		this.hover = hover;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public void setReachable(boolean reachable) {
		this.reachable = reachable;
	}

	public Actor getActorOnTile() {
		return actorOnTile;
	}

	/**
	 * 
	 * @param newActor
	 *            Actor to place on tile
	 * @return the actor previously on tile, or null if no actor present
	 */
	public Actor setActorOnTile(Actor newActor) {
		Actor oldActor = actorOnTile;
		actorOnTile = newActor;
		return oldActor;
	}

	/**
	 * @return the nextUp
	 */
	public Tile getNextUp() {
		return nextUp;
	}

	/**
	 * @return the nextDown
	 */
	public Tile getNextDown() {
		return nextDown;
	}

	/**
	 * @return the nextLeft
	 */
	public Tile getNextLeft() {
		return nextLeft;
	}

	/**
	 * @return the nextRight
	 */
	public Tile getNextRight() {
		return nextRight;
	}

	/**
	 * @param nextUp
	 *            the nextUp to set
	 */
	public void setNextUp(Tile nextUp) {
		this.nextUp = nextUp;
		setAdjacentTiles();
	}

	/**
	 * @param nextDown
	 *            the nextDown to set
	 */
	public void setNextDown(Tile nextDown) {
		this.nextDown = nextDown;
		setAdjacentTiles();
	}

	/**
	 * @param nextLeft
	 *            the nextLeft to set
	 */
	public void setNextLeft(Tile nextLeft) {
		this.nextLeft = nextLeft;
		setAdjacentTiles();
	}

	/**
	 * @param nextRight
	 *            the nextRight to set
	 */
	public void setNextRight(Tile nextRight) {
		this.nextRight = nextRight;
		setAdjacentTiles();
	}
	
	private void setAdjacentTiles() {
		this.adjacentTiles[0] = this.nextUp;
		this.adjacentTiles[1] = this.nextDown; 
		this.adjacentTiles[2] = this.nextLeft;
		this.adjacentTiles[3] = this.nextRight; 
	}

	/**
	 * @return the type
	 */
	@Override
	public TileType getType() {
		return (TileType) type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	@Override
	public void setType(GameObjectType type) {
		if (type instanceof TileType) {
			this.type = type;
		} else {
			throw new IllegalArgumentException(
					"Type for Tile must be an instance of TileType.");
		}
	}
}
