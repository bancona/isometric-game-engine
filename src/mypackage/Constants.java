/**
 * @author Bertie Ancona
 */
package mypackage;

public class Constants {
	public static int FPS = 60;
	public static int NS_PER_FRAME = (int) 1e9 / FPS;
}
