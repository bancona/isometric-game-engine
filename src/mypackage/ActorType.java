/**
 * @author Bertie Ancona
 */
package mypackage;

public class ActorType extends GameObjectType {
	public final double speed;
	public final int baseHP;
	public final int attackRange;
	public final int attackDamage;

	/**
     * @param name name of the actor type
     * @param texture name of the texture the actor uses as an image
	 * @param speed number of tiles the actor can travel in one turn
	 * @param baseHP starting number of hit points the actor has
	 * @param attackRange the number of tiles from which an actor
	 * @param attackDamage the damage the actor can inflict on another in one
     *                     turn
	 */
	private ActorType(String name, String texture, double speed,
			int baseHP, int attackRange, int attackDamage) {
		super(name, texture);
		this.speed = speed;
		this.baseHP = baseHP;
		this.attackRange = attackRange;
		this.attackDamage = attackDamage;
	}

}
