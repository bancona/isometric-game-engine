/**
 * @author Bertie Ancona
 */
package mypackage;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureImpl;

public abstract class GameObject {
	protected GameObjectType type;
    protected Texture texture;
    protected double texHalfWidth;
    protected double texHalfHeight;
    protected double[] vertCoords;

	public GameObject(GameObjectType type) {
        this.type = type;
        this.texture = getType().getTextureObj().getTexture();
        this.texHalfWidth = texture.getTextureWidth() * 0.125;
        this.texHalfHeight = texture.getTextureHeight() * 0.125;
        this.vertCoords = new double[] {
                -texHalfWidth, Tile.HALF_HEIGHT - texHalfHeight,
                texHalfWidth,  Tile.HALF_HEIGHT - texHalfHeight,
                texHalfWidth,  Tile.HALF_HEIGHT + texHalfHeight,
                -texHalfWidth, Tile.HALF_HEIGHT + texHalfHeight
        };
	}

	protected void paint(double[] texCoords) {
        if (texCoords.length < 8)
            throw new IllegalArgumentException("Number of texture coordinates" +
                    " is fewer than needed for 4 vertices; eight (8) numbers " +
                    "required. Number provided: " + texCoords.length);

        GL11.glPushMatrix();
        texture.bind();

		GL11.glBegin(GL11.GL_POLYGON);
		for (int x = 0, y = 1; y < 8; x += 2, y += 2) {
			GL11.glTexCoord2d(texCoords[x], texCoords[y]);
			GL11.glVertex2d(vertCoords[x], vertCoords[y]);

        }
		GL11.glEnd();
		TextureImpl.bindNone();
		GL11.glPopMatrix();
	}

	public List<String> getHUDDescriptors() {
		ArrayList<String> descriptors = new ArrayList<String>();
		descriptors.add(this.getType().getName().toUpperCase());
		return descriptors;
	}

	public abstract GameObjectType getType();

	public abstract void setType(GameObjectType type);

}