/**
 * @author Bertie Ancona
 */
package mypackage;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class GameTexture {

	protected Texture texture;
	protected String name;

    protected double[][] texCoords = new double[][] {
            // regular for tile, north for actor
            new double[] { 0.0, .75, .25, .75, .25, 1.0, 0.0, 1.0 },
            // hover for tile, northwest for actor
            new double[] { 0.0, .50, .25, .50, .25, .75, 0.0, .75 },
            // west for actor
            new double[] { 0.0, .25, .25, .25, .25, .50, 0.0, .50 },
            // southwest for actor
            new double[] { 0.0, 0.0, .25, 0.0, .25, .25, 0.0, .25 },
            // reachable for tile, south for actor
            new double[] { .25, .75, .50, .75, .50, 1.0, .25, 1.0 },
            // northeast for actor
            new double[] { .25, .50, .50, .50, .50, .75, .25, .75 },
            // east for actor
            new double[] { .25, .25, .50, .25, .50, .50, .25, .50 },
            // southeast for actor
            new double[] { .25, 0.0, .50, 0.0, .50, .25, .25, .25 },
    };

    public static GameTexture get(String name) {
        if (!loaded) {
            loadTexturesFromFile();
        }
        GameTexture textureToGet = textures.get(name);
        if (textureToGet.getTexture() == null) {
            textureToGet.loadTexture();
        }
        return textureToGet;
    }

	protected GameTexture(String name) {
		this.name = name;
	}

	public Texture getTexture() {
		return texture;
	}
	
	public String getName() {
		return name;
	}

    public double[] getTexCoords(TileState state) {
        switch (state) {
            case REACHABLE:
            case IN_FOCUS:
                return texCoords[7];
            case HOVER:
                return texCoords[2];
            case REGULAR:
            default:
                return texCoords[3];
        }
    }

    public double[] getTexCoords(Direction direction) {
        switch (direction) {
            case NORTH:
                return texCoords[0];
            case NORTH_WEST:
                return texCoords[1];
            case WEST:
                return texCoords[2];
            case SOUTH_WEST:
                return texCoords[3];
            case SOUTH:
                return texCoords[4];
            case NORTH_EAST:
                return texCoords[5];
            case EAST:
                return texCoords[6];
            case SOUTH_EAST:
            default:
                return texCoords[7];
        }
    }

    private static Map<String, GameTexture> textures;
    private static boolean loaded = false;

	protected void loadTexture() {
		try {
			texture = TextureLoader.getTexture("PNG", getClass()
					.getResourceAsStream("/images/" + name + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
			Display.destroy();
		}
	}

    public static void loadTexturesFromFile() {
        if (loaded) {
            return;
        }
        loaded = true;
        textures = new HashMap<>();
        String typesJSON = null;
        try {
            typesJSON = new String(Files.readAllBytes(Paths.get(GameTexture
                    .class.getResource("/json/gametextures.json").toURI())));
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            Display.destroy();
            System.exit(1);
        }
        JsonArray jsonTextures = new JsonParser().parse(typesJSON)
                .getAsJsonObject().get("textures").getAsJsonArray();
        for (int i = 0; i < jsonTextures.size(); ++i) {
            GameTexture gameTexture = new GameTexture(jsonTextures.get(i)
                    .getAsJsonObject().get("name").getAsString());
            textures.put(gameTexture.getName(), gameTexture);
        }
    }
}
