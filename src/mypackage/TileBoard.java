/**
 * @author Bertie Ancona
 */
package mypackage;

import java.util.Set;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

public class TileBoard {

	public final Tile[][] tiles;
	public final double WIDTH, HEIGHT;

	/**
	 * @param length
	 *            the number of tiles per side of the square board.
	 */
	public TileBoard(int length) {
		tiles = new Tile[length][length];
		WIDTH = Tile.WIDTH * length;
		HEIGHT = Tile.HEIGHT * length;
		for (int i = 0; i < length; ++i) {
			for (int j = 0; j < length; ++j) {
				tiles[i][j] = new Tile((TileType) GameObjectType.get("grass"));
			}
		}
		linkTiles();
	}

	private void linkTiles() {
		for (int i = 0; i < tiles.length; ++i) {
			for (int j = 0; j < tiles.length; ++j) {
				tiles[i][j].setNextUp(getTile(i, j + 1));
				tiles[i][j].setNextDown(getTile(i, j - 1));
				tiles[i][j].setNextLeft(getTile(i - 1, j));
				tiles[i][j].setNextRight(getTile(i + 1, j));
			}
		}
	}

	public void updateTiles() {
		unHoverAll();
		Set<Tile> reachableTiles = null;
		Tile tileUnderMouse = getTileUnderMouse();
		Tile selectedActorTile = getSelectedActorTile();
		if (tileUnderMouse != null) {
			tileUnderMouse.setHover(true);
			while (Mouse.next()) {
				selectedActorTile = getSelectedActorTile();
				reachableTiles = selectedActorTile == null ? null
						: selectedActorTile.getReachableTiles(selectedActorTile
								.getActorOnTile().getType().speed);
				// if left button press
				if (Mouse.getEventButton() == 0 && Mouse.getEventButtonState()) {
					// left click on tile with selected actor
					if (HUD.getGameObjectInFocus() == tileUnderMouse
							.getActorOnTile()) {
						HUD.setGameObjectInFocus(tileUnderMouse);
					}
					// a different tile has selected actor, and hovering tile
					// has
					// no actor and is reachable by selected actor
					else if (HUD.getGameObjectInFocus() instanceof Actor
							&& tileUnderMouse.getActorOnTile() == null
							&& reachableTiles != null
							&&reachableTiles.contains(tileUnderMouse)) {
						// move actor to tileUnderMouse
						tileUnderMouse.setActorOnTile(selectedActorTile
                                .setActorOnTile(null));
					} else if (!(HUD.getGameObjectInFocus() instanceof Actor)
							&& tileUnderMouse.getActorOnTile() != null) {
						HUD.setGameObjectInFocus(tileUnderMouse
								.getActorOnTile());
					} else if (tileUnderMouse.getActorOnTile() == null) {
						boolean wasSelected = tileUnderMouse == HUD
								.getGameObjectInFocus();

						HUD.setGameObjectInFocus(wasSelected ? null
								: tileUnderMouse);
					} else if (tileUnderMouse.getActorOnTile() != null) {
						HUD.setGameObjectInFocus(tileUnderMouse
								.getActorOnTile());
					}
				}
			}
		}
		if (reachableTiles != null) {
			unReachableAll();
			for (Tile reachableTile : reachableTiles) {
				reachableTile.setReachable(true);
			}
		} else if (selectedActorTile == null) {
			unReachableAll();
		}
	}

	/**
	 * Paint tiles from top to bottom.
	 */
	public void paintTiles() {
		for (int layer = 0; layer <= 2 * (tiles.length - 1); ++layer) {
			int yMin = Math.max(0, layer - tiles.length + 1);
			int yMax = Math.min(tiles.length - 1, layer);
			for (int y = yMin; y <= yMax; ++y) {
				int x = layer - y;
				GL11.glPushMatrix();
				GL11.glTranslated(
						0.5 * ((x - y) * Tile.WIDTH + Display.getWidth()),
						0.5 * ((layer - 1) * Tile.HEIGHT + Display.getHeight() - HEIGHT),
						0);
				tiles[x][y].paint();
				GL11.glPopMatrix();
			}
		}
	}

	public Tile getSelectedActorTile() {
		for (Tile[] row : tiles) {
			for (Tile tile : row) {
				if (tile.getActorOnTile() != null
						&& tile.getActorOnTile() == HUD.getGameObjectInFocus()) {
					return tile;
				}
			}
		}
		return null;
	}

	public void unHoverAll() {
		for (Tile[] row : tiles) {
			for (Tile tile : row) {
				tile.setHover(false);
			}
		}
	}

	public void unReachableAll() {
		for (Tile[] row : tiles) {
			for (Tile tile : row) {
				tile.setReachable(false);
			}
		}
	}

	public void unSelectAll() {
		for (Tile[] row : tiles) {
			for (Tile tile : row) {
				tile.setSelected(false);
			}
		}
	}

	public Tile getTile(int x, int y) {
		try {
			return tiles[x][y];
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	public Tile getTileUnderMouse() {
		double mouseWorldX = -Mouse.getX() + Camera.getX() + 0.5
				* IsoGame.WINDOW_WIDTH;
		double mouseWorldY = Mouse.getY() + Camera.getY() - 0.5
				* IsoGame.WINDOW_HEIGHT;
		double mouseGridX = (1 - 0.5 * (2 * mouseWorldY + mouseWorldX + HEIGHT)
				/ HEIGHT)
				* tiles.length;
		double mouseGridY = (1 - 0.5 * (2 * mouseWorldY - mouseWorldX + HEIGHT)
				/ HEIGHT)
				* tiles.length;
		if (mouseGridX >= 0 && (int) mouseGridX < tiles.length
				&& mouseGridY >= 0 && (int) mouseGridY < tiles.length) {
			return tiles[(int) mouseGridX][(int) mouseGridY];
		}
		return null;
	}
}
