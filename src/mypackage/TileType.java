/**
 * @author Bertie Ancona
 */
package mypackage;

public class TileType extends GameObjectType {
	public final double traversalTime;

    /**
     * @param name name of the tile type
     * @param texture name of the texture the tile uses as an image
     * @param traversalTime number of turns it takes an actor to traverse the
     *                      tile at normal speed
     */
	private TileType(String name, String texture, double traversalTime) {
		super(name, texture);
		this.traversalTime = traversalTime;
	}

}