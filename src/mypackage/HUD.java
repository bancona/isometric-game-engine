/**
 * @author Bertie Ancona
 */
package mypackage;
import java.awt.Font;
import java.util.List;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.opengl.TextureImpl;

public class HUD {
    public static final int HEIGHT = 150;

    private static GameObject gameObjectInFocus;
    private static TrueTypeFont font = new TrueTypeFont(new Font("Arial", Font.BOLD, 15), false);

    public static void paint() {
        if (gameObjectInFocus != null) {
            drawBackground();
            writeGameObjectInfo();
        }
    }

    private static void writeGameObjectInfo() {
        GL11.glPushMatrix();

        TextureImpl.bindNone();
        Color.white.bind();

        GL11.glTranslated(-Camera.getX(), Display.getHeight() - Camera.getY()
                - HEIGHT, 0);

        List<String> descriptors = gameObjectInFocus.getHUDDescriptors();
        for (int i = 0; i < descriptors.size(); ++i) {
            font.drawString(20, font.getLineHeight() * (i + 1),
                    descriptors.get(i));
        }

        GL11.glPopMatrix();
    }

    private static void drawBackground() {
        GL11.glPushMatrix();
        // saves current color
        GL11.glPushAttrib(GL11.GL_CURRENT_BIT);

        // move so that HUD is always in the same place
        GL11.glTranslated(-Camera.getX(), Display.getHeight() - Camera.getY(),
                0);
        // set color to gray
        GL11.glColor4d(.5, .5, .5, .6);

        // draw rectangle across whole display
        GL11.glBegin(GL11.GL_QUADS);
        GL11.glVertex2d(0, 0);
        GL11.glVertex2d(Display.getWidth(), 0);
        GL11.glVertex2d(Display.getWidth(), -HEIGHT);
        GL11.glVertex2d(0, -HEIGHT);
        GL11.glEnd();

        // reverts to previous color
        GL11.glPopAttrib();
        GL11.glPopMatrix();
    }

    /**
     * @return the gameObjectInFocus
     */
    public static GameObject getGameObjectInFocus() {
        return gameObjectInFocus;
    }

    /**
     * @param gameObjectInFocus
     *            the gameObjectInFocus to set
     */
    public static void setGameObjectInFocus(GameObject gameObjectInFocus) {
        HUD.gameObjectInFocus = gameObjectInFocus;
    }
}