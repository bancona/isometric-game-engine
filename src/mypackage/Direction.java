/**
 * @author Bertie Ancona
 */
package mypackage;
public enum Direction {
	NORTH, SOUTH, EAST, WEST, NORTH_EAST, SOUTH_EAST, NORTH_WEST, SOUTH_WEST;
}
