/**
 * @author Bertie Ancona
 */
package mypackage;

import java.util.Collections;
import java.util.List;

public class Actor extends GameObject {
	protected boolean alive;
	protected int hitpoints;
	protected boolean selected;
	protected Direction direction;

	public Actor(ActorType type) {
		super(type);
		alive = true;
		selected = false;
		hitpoints = type.baseHP;
		direction = Direction.SOUTH;
	}

	public void act() {

	}

	public void paint() {
		double[] texCoords = getType().getTextureObj().getTexCoords(direction);
		paint(texCoords);
	}

	/**
	 * Heals actor by addedHealth hitpoints
	 * 
	 * @param addedHealth
	 *            amount to heal the actor
	 */
	public void heal(int addedHealth) {
		hitpoints += addedHealth;
	}

	/**
	 * Damages actor by lostHealth hitpoints
	 * 
	 * @param lostHealth
	 *            amount to damage the actor
	 */
	public void damage(int lostHealth) {
		hitpoints -= lostHealth;
		if (hitpoints <= 0) {
			alive = false;
			die();
		}
	}

	public void die() {

	}

	@Override
	public List<String> getHUDDescriptors() {
		List<String> descriptors = super.getHUDDescriptors();
		Collections.addAll(descriptors,
				String.format("Hitpoints: %s/%s", hitpoints, getType().baseHP),
				String.format("Speed: %s", getType().speed),
				String.format("Attack Range: %s", getType().attackRange),
				String.format("Attack Damage: %s", getType().attackDamage));
		return descriptors;
	}

	/**
	 * @return the alive
	 */
	public boolean isAlive() {
		return alive;
	}

	/**
	 * @return the hitpoints
	 */
	public int getHitpoints() {
		return hitpoints;
	}

	/**
	 * @return the type
	 */
	@Override
	public ActorType getType() {
		return (ActorType) type;
	}

	/**
	 * @return the selected
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * @return the direction
	 */
	public Direction getDirection() {
		return direction;
	}

	/**
	 * @param alive
	 *            the alive to set
	 */
	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	/**
	 * @param hitpoints
	 *            the hitpoints to set
	 */
	public void setHitpoints(int hitpoints) {
		this.hitpoints = hitpoints;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	@Override
	public void setType(GameObjectType type) {
		if (type instanceof ActorType) {
			this.type = type;
		} else {
			throw new IllegalArgumentException(
					"Type for Actor must be an instance of ActorType.");
		}
	}

	/**
	 * @param selected
	 *            the selected to set
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	/**
	 * @param direction
	 *            the direction to set
	 */
	public void setDirection(Direction direction) {
		this.direction = direction;
	}

}
