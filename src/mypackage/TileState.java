/**
 * @author Bertie Ancona
 */
package mypackage;

public enum TileState {
	REGULAR, HOVER, REACHABLE, IN_FOCUS
}
