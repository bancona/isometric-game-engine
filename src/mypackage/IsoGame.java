/**
 * @author Bertie Ancona
 */
package mypackage;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

public class IsoGame {

	public static final boolean DEBUG = true;

	public static final int WINDOW_WIDTH = 1000;
	public static final int WINDOW_HEIGHT = 600;

	public static final int BOARD_LENGTH = 11;

	public static void main(String[] args) {
		initOpenGL();
		loadTypesAndTextures();

		// create game board
		TileBoard board = new TileBoard(BOARD_LENGTH);
		// add knight to bottom tile
		board.tiles[BOARD_LENGTH - 1][BOARD_LENGTH - 1]
				.setActorOnTile(new Actor((ActorType) GameObjectType.get
                        ("knight")));
		// center camera on board
		Camera.setValues(0, 0, 15, board);

		playGame(board);

		Display.destroy();
	}

	private static void loadTypesAndTextures() {
        GameObjectType.loadTypes();
		GameTexture.loadTexturesFromFile();
	}

	private static void playGame(TileBoard board) {
		while (!Display.isCloseRequested()) {
			double start = System.nanoTime();
			// run game logic and display
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
			board.updateTiles();
			board.paintTiles();
			HUD.paint();
			Display.update();
			Camera.move();

			// clamp to specified frame rate
			Display.sync(Constants.FPS);

			// set delta
			int dt = (int) Math.abs(System.nanoTime() - start);

			if (DEBUG) {
				Display.setTitle("FPS: " + (int) (1e9 / dt));
			}
		}
	}

	private static void initOpenGL() {
		// create display
		try {
			Display.setDisplayMode(new DisplayMode(WINDOW_WIDTH, WINDOW_HEIGHT));
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
			Display.destroy();
		}

		// set clear color to black
		GL11.glClearColor(0, 0, 0, 1);
		GL11.glShadeModel(GL11.GL_SMOOTH);

		// enable alpha blending
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

		GL11.glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(0, WINDOW_WIDTH, WINDOW_HEIGHT, 0, 1, -1);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
	}
}