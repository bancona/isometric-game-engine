/**
 * @author Bertie Ancona
 */
package mypackage;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

public class Camera {
	private static double x, y, speed;
	private static double MIN_X, MAX_X, MIN_Y, MAX_Y;

	public static void setValues(double x, double y, double speed,
			TileBoard board) {
		Camera.x = x;
		Camera.y = y;
		Camera.speed = speed;
		MAX_X = 0.5 * (board.WIDTH - IsoGame.WINDOW_WIDTH);
		MIN_X = -0.5 * (board.WIDTH - IsoGame.WINDOW_WIDTH);
		MAX_Y = 0.5 * board.HEIGHT;
		MIN_Y = -0.5 * (board.HEIGHT);// - IsoGame.WINDOW_HEIGHT);
		//System.out.println("maxx = " + MAX_X);
		//System.out.println("minx = " + MIN_X);
		//System.out.println("maxy = " + MAX_Y);
		//System.out.println("miny = " + MIN_Y);
	}

	/**
	 * Moves camera in response to arrow keys. Movement is limited to within the
	 * rectangular bounds of the tile board.
	 */
	public static void move() {
		double dx = 0, dy = 0;
		if (Keyboard.isKeyDown(Keyboard.KEY_UP)) {
			dy += Math.min(MAX_Y - y, speed);
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
			dy += Math.max(MIN_Y - y, -speed);
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
			dx += -Math.max(x - MAX_X, -speed);
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
			dx += -Math.min(x - MIN_X, speed);
		}
		x += dx;
		y += dy;
		GL11.glTranslated(dx, dy, 0);
	}

	public static void reset() {
		GL11.glTranslated(-x, -y, 0);
		x = y = 0;
	}

	/**
	 * @return the x
	 */
	public static double getX() {
		return x;
	}

	/**
	 * @return the y
	 */
	public static double getY() {
		return y;
	}

}
