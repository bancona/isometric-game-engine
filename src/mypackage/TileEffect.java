/**
 * @author Bertie Ancona
 */
package mypackage;
public enum TileEffect {
	FLAME(false), ICE(false), POISON(true);
	public final boolean canStack;

	TileEffect(boolean canStack) {
		this.canStack = canStack;
	}
}
